#!/usr/bin/env python

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait
import time

# Import os so that we can grab username and password from commandline
import os


driver = webdriver.Firefox()

# Setup variables
# command line:  export AMAZON_PASSWORD='somepassword'
passwd_var = os.getenv('AMAZON_PASSWORD', 'forgot: export AMAZON_PASSWORD="blah"')
user_var = os.getenv('AMAZON_USER', 'forgot: export AMAZON_USER="blah"')
amazon_gift_card_page = 'https://www.amazon.com/gp/product/B0145WHYKC/gcrnsts?ie=UTF8&ref_=s9_acss_bw_cg_gifting_3b1'
action = webdriver.ActionChains(driver)

# Open Amazon webpage
driver.get("https://www.amazon.com")


time.sleep(2)

# make sure we are actually on amazons web page
assert "Amazon" in driver.title



# Login to amazon.com
driver.find_element_by_link_text('Sign in').click()
time.sleep(2)
email_elem = driver.find_element_by_id('ap_email')
email_elem.send_keys(user_var)
passwd_elem = driver.find_element_by_id('ap_password')
passwd_elem.send_keys(passwd_var)
passwd_elem.submit()

# Load the amazon gift card page
driver.get(amazon_gift_card_page)



# Fill out gift card order form
amount_elem = driver.find_element_by_id('gc-order-form-custom-amount')
amount_elem.send_keys('0.50')
recipt_elem = driver.find_element_by_id('gc-order-form-recipients')
recipt_elem.send_keys(user_var)
driver.find_element_by_id('gc-order-form-senderName').send_keys(' from me')

time.sleep(2)

# Scroll to the proceed to checkout button and click it
button = WebDriverWait(driver, 20).until(expected_conditions.presence_of_element_located((By.ID, 'gc-mini-cart-proceed-to-checkout')))
hover = ActionChains(driver).move_to_element(button)
driver.execute_script("arguments[0].scrollIntoView(true);", button);
hover.perform()
button.click()

# change to the prefered payment method
driver.find_element_by_xpath("//strong[contains(text(), 'MasterCard')]").click();

# scroll to the Continue to purchase button and click it.
button = WebDriverWait(driver, 20).until(expected_conditions.presence_of_element_located((By.ID, 'continue-top')))
hover = ActionChains(driver).move_to_element(button)
driver.execute_script("arguments[0].scrollIntoView(true);", button);
hover.perform()
button.click()
