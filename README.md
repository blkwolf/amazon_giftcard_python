## Amazon Gift Card

Based off of ageoldpun's node script: https://github.com/ageoldpun/amazon_gifts

Python script using  selenium-webdriver to purchase yourself a single, $0.50 Amazon gift card.

## To Use

Requires Python, Selenium, webdriver, and Firefox
pip install selenium
pip install webdriver

Before running set the following environment variables:

export AMAZON_USERNAME='<your email address for amazon.com>'
export AMAZON_PASSWORD='<your amazon password>'

You will also probably want to change MasterCard in line 63 to match your payment method

## To run
python ./amazongc.py


## Note:
The scripts stops at the final check out page to allow you to verify the information and complete the purchase manually.
